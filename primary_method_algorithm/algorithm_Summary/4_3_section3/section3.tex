\expandafter\ifx\csname ifdraft\endcsname\relax
  \newcommand{\ifdrafta}{false}
  \newcommand{\ifdraftb}{false}
  \input{../Setting/1_preamble/preamble}
  \input{../Setting/2_command/command}
  \begin{document}
\fi

\section{近接勾配法(Proximal Gradient Method)}

    微分可能な凸関数$f(\bm{x})$と微分不可能な点を含む凸関数$g(\bm{x})$に対して
        \begin{equation}
            F(\bm{x}) = f(\bm{x}) + g(\bm{x})
            \label{eq:Proximal_1}
        \end{equation}
        を最小化するアルゴリズム．

    手順は以下の通り(直感的理解ができるように書いた)：
        \begin{framed}
        \begin{enumerate}
            \item 現在の$\bm{x}$から$\nabla f(\bm{x})$の逆方向にステップサイズ$\eta$で移動(移動先を仮に$\bm{x}'$とする)
            \begin{equation}
                \bm{x}' = \bm{x} - \eta \nabla f(\bm{x})
            \end{equation}
            \item $\bm{x}'$の周りで$g(\bm{x})$を最小化する点$\bm{x}$を探す．\\
            この時，$\bm{x}'$から離れるほど$f(\bm{x})$の値は変化していってしまうので，$\bm{x}'$から離れるほど損するように罰則項$||\bm{x} - \bm{x}'||^2/2$が追加される(！この値はデタラメではない！後で記述)．
            \begin{equation}
                \text{prox}_g(\bm{x}') \equiv \text{argmin}_{\bm{x}} \left\{ g(\bm{x}) + \frac{1}{2}||\bm{x} - \bm{x}'||^2 \right\}
            \end{equation}
        \end{enumerate}
      \end{framed}

    実際には便宜上，少し工夫がある：
        \begin{align}
            \bm{x}_{k+1} &= \text{prox}_{\eta g}\left( \bm{x}_k - \eta \nabla f(\bm{x}_k) \right) \\
            &= \text{argmin}_{\bm{x}} \left( \eta g(\bm{x}) + \frac{1}{2}\left|\left|\bm{x} - (\bm{x}_k - \eta \nabla f(\bm{x}_k))\right|\right|^2 \right)\\
            &= \text{argmin}_{\bm{x}} \left( g(\bm{x}) + \frac{1}{2\eta} \left|\left| \bm{x} - \bm{x}_k + \eta \nabla f(\bm{x}_k) \right|\right|^2 \right)\\
            &= \text{argmin}_{\bm{x}} \left(g(\bm{x}) + \frac{1}{2\eta}\left\{ (\bm{x}-\bm{x}_k)+ \eta \nabla f(\bm{x}_k) \right\}^{\top}\left\{ (\bm{x}-\bm{x}_k)+ \eta \nabla f(\bm{x}_k) \right\}\right)\\
            &= \text{argmin}_{\bm{x}} \left(g(\bm{x}) + \frac{1}{2\eta}\left\{ ||\bm{x}-\bm{x}_k||^2 + 2\eta\nabla f(\bm{x}_k)^{\top}(\bm{x}-\bm{x}_k) + || \eta\nabla f(\bm{x}_k) ||^2 \right\} \right)\\
            &= \text{argmin}_{\bm{x}} \left( g(\bm{x}) + \nabla f(\bm{x}_k)^{\top}(\bm{x}-\bm{x}_k) + \frac{1}{2\eta}||\bm{x}-\bm{x}_k||^2 \right)\\
            &= \text{argmin}_{\bm{x}} \left( g(\bm{x}) + f(\bm{x}_k) + \nabla f(\bm{x}_k)^{\top}(\bm{x}-\bm{x}_k) + \frac{1}{2\eta}||\bm{x}-\bm{x}_k||^2 \right)
            \label{eq:gamma}
        \end{align}

    ここで，関数$f$が，ある$L\geq 0$が存在し，任意の$\bm{x}, \bm{x}'$に対して常に
        \begin{equation}
            |f(\bm{x}) - f(\bm{x}')|\leq L||\bm{x} - \bm{x}'||
        \end{equation}
        を満たすとき，$f$はリプシッツ連続であるといい，定数$L$をリプシッツ定数と言う．
        また，勾配$\nabla f$がリプシッツ定数$\gamma$のリプシッツ連続であることを$\gamma$-平滑という：
        \begin{equation}
            \textbf{[$\gamma$-平滑]}\quad \quad
            ||\nabla f(\bm{x}) - \nabla f(\bm{x}')|| \leq \gamma||\bm{x} - \bm{x}'||
        \end{equation}
        この時，以下の式が成り立つ：
        \begin{align}
            f(\bm{x}) &= f(\bm{x}') + \int_0^1 \nabla f(\bm{x}' + t(\bm{x} - \bm{x}'))^{\top} (\bm{x} - \bm{x}')dt\\
            &= f(\bm{x}') + \nabla f(\bm{x}')^{\top}(\bm{x}-\bm{x}') + \int_0^1 \left\{ \nabla f(\bm{x}'+t(\bm{x} - \bm{x}')) - \nabla f(\bm{x}')\right\}^{\top}(\bm{x}-\bm{x}')dt\\
            &\leq
            f(\bm{x}') + \nabla f(\bm{x}')^{\top}(\bm{x}-\bm{x}') + \int_0^1 ||\nabla f(\bm{x}' + t(\bm{x}-\bm{x}')) - \nabla f(\bm{x}')||\cdot ||\bm{x}-\bm{x}'||dt\\
            &\leq
            f(\bm{x}') + \nabla f(\bm{x}')^{\top}(\bm{x}-\bm{x}') + \int_0^1 \gamma ||t(\bm{x}-\bm{x}')||\cdot ||\bm{x}-\bm{x}'||dt\textbf{（$\gamma$-平滑であることから）}\\
            &= f(\bm{x}') + \nabla f(\bm{x}')^{\top}(\bm{x}-\bm{x}') + \frac{\gamma}{2}||\bm{x}-\bm{x}'||^2
            \label{eq:jogen}
        \end{align}

    ここで，式(\ref{eq:gamma})を再度見ると，$g(\bm{x})$以降の項と一致していることが分かる．
        すなわち，式(\ref{eq:gamma})の後半の項は，$f$が$1/\eta$-平滑である際に，$f(\bm{x})$の上限を表している事になる．
        ただし，この際のリプシッツ定数がステップサイズになっていることが特徴．
        このため，ステップサイズは適切に設定しなければならない．

    リプシッツ定数$\gamma$は小さいほど望ましい．
        なぜなら，$f$の上限を狭めることができるから．
        すなわち，ステップサイズ$\eta$をできるだけ大きくしたいことと対応する．
        直感的にも納得．
        実際には，$\gamma$の計算は難しいため，各ステップにおいて，
        \begin{align}
            f(\bm{x}_{k+1}) &\leq f(\bm{x}_k) + \nabla f(\bm{x}_k)^{\top}(\bm{x}_{k+1} - \bm{x}_k) + \frac{1}{2\eta}||\bm{x}_{k+1} - \bm{x}_k||^2\\
            &= f(\bm{x}_k) + \nabla f(\bm{x}_k)^{\top}\left(-\eta\nabla f(\bm{x}_k)\right) + \frac{1}{2\eta}||-\eta \nabla f(\bm{x}_k)||^2\\
            &= f(\bm{x}_k) - \eta ||\nabla f(\bm{x}_k)||^2 + \frac{\eta}{2}||\nabla f(\bm{x}_k)||^2\\
            &= f(\bm{x}_k) - \frac{\eta}{2}||\nabla f(\bm{x}_k)||^2
            \label{eq:saki}
        \end{align}
        を満たす$\eta$を見つけるまで$\eta \Leftarrow \beta\eta(0<\beta<1)$を繰り返す．
        (他はどうでもいいけど，少なくとも移動後の点では式(\ref{eq:jogen})を満たすようにしようね！っていう考え方．ステップサイズが$f$全体の平滑度を表しているわけではないので注意．一般に"backtracking"と呼ばれる)





    \begin{algorithm}
        \caption{Proximal Gradient Method ($g(\bm{x})=0$の場合)}
        \label{algo:Momentum}
        \begin{algorithmic}[1]
            \REQUIRE $\epsilon \geq 0, \eta_0 \geq 0, 0<\beta<1, f, \nabla f, \bm{x_0}$
            \ENSURE $\bm{x}^*$

            \WHILE{}
                \STATE Calculate $f(\bm{x}_k), \nabla f(\bm{x}_k)$
                \WHILE{}
                    \STATE $\bm{x}_{k+1} = \bm{x}_k - \eta \nabla f(\bm{x}_k)$
                    \IF{式(\ref{eq:saki})を満たす}
                        \STATE break
                    \ENDIF
                    \STATE $\eta \Leftarrow \beta\eta$
                \ENDWHILE
                \STATE $k \Leftarrow k+1$
                \STATE (収束判定値)の計算
                \IF{(収束判定値)$\leq \epsilon$}
                    \STATE break
                \ENDIF
            \ENDWHILE
            \RETURN $\bm{x}^* = \bm{x}_k$
        \end{algorithmic}
    \end{algorithm}

\expandafter\ifx\csname ifdraft\endcsname\relax
  \bibliographystyle{../Setting/sty/jsce}
  \bibliography{../5_bibliography/Bibliography/TexBib}
  \end{document}
\fi
