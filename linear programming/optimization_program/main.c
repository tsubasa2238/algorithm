#include <stdio.h>
#include <stdlib.h>
#include <float.h>
#include <math.h>
#include "matrix.h"
#include "linear.h"

double function_1(const double* x){

    double obj = 0.0;

    for(int i=0; i<10; i++){
        // obj += x[i]*x[i];
        obj += x[i]*x[i] + x[i] + exp(x[i]);
    }

    return obj;

}

double* function_nabla_1(const double* x){

    double* nabla = malloc(sizeof(double)*10);

    zeros_vector_double(nabla, 10);

    for(int i=0; i<10; i++){
        // nabla[i] = 2*x[i];
        nabla[i] = 2*x[i] + 1 + exp(x[i]);
    }


    return nabla;

}




void main(){

    /*
    線形計画問題の情報を取得 ーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーー

    a_matrix：制約条件の係数行列
    b_matrix[0]：制約条件の右辺のベクトル
    c_matrix[0]：目的関数の係数行列

    */
    char *filename;
    filename = "csv/A.csv";

    int num_x;
    int num_b;

    double** a_matrix;
    int num_row;
    int num_col;

    read_matrix_csv(filename, &a_matrix, &num_row, &num_col);
    // printf_matrix_double(a_matrix, num_row, num_col);
    num_b = num_row;

    filename = "csv/c.csv";
    double** c_matrix;
    read_matrix_csv(filename, &c_matrix, &num_row, &num_col);
    // printf_vector_double(c_matrix[0], num_col);
    num_x = num_col;

    filename = "csv/b.csv";
    double** b_matrix;
    read_matrix_csv(filename, &b_matrix, &num_row, &num_col);
    // printf_vector_double(b_matrix[0], num_col);
    // printf("\n\n");


    /*
    シンプレックス法ーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーー
    */
    simplex_nomal(c_matrix[0], a_matrix, b_matrix[0], num_x, num_b);


    free(a_matrix);
    free(b_matrix);
    free(c_matrix);

}