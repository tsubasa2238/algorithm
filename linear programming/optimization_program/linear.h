#ifndef _LINEAR_H_
#define _LINEAR_H_


/*
単体法(シンプレックス法)
第一引数：目的関数の係数ベクトル(定数項は除く)のポインタ，第二引数：制約条件の係数行列のポインタ，第三引数：制約条件の右辺のベクトル，
第四引数：変数の要素数(cの要素数)，第五引数：制約条件の数(bの要素数)
返り値：0
*/
int simplex_nomal(double* const c, double** const a, double* const b, const int num_x, const int num_b);


#endif // _LINEAR_H_