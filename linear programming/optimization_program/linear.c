#include "linear.h"
#include "matrix.h"
#include <stdio.h>
#include <stdlib.h>
#include <float.h>
#include <math.h>


/*
単体法(シンプレックス法)
第一引数：目的関数の係数ベクトル(定数項は除く)のポインタ，第二引数：制約条件の係数行列のポインタ，第三引数：制約条件の右辺のベクトル，
第四引数：変数の要素数(cの要素数)，第五引数：制約条件の数(bの要素数)
返り値：0
*/
int simplex_nomal(double* const c, double** const a, double* const b, const int num_x, const int num_b){

    double z = 0.0;

    double* n_index = malloc(sizeof(double)*num_x);
    double* b_index = malloc(sizeof(double)*num_b);

    index_vector_double(n_index, num_x);
    for(int j = 0; j < num_b; j++){
        b_index[j] = (double)num_x + j;
    }

    double max;
    double min;

    // 1
    while(1){

        // printf("a_");
        // printf_matrix_double(a, num_b, num_x);
        // printf("b_");
        // printf_vector_double(b, num_b);
        // printf("c_");
        // printf_vector_double(c, num_x);

        // printf("zero vector\n");
        // printf_vector_double(n_index, num_x);
        // printf("nonzero vector\n");
        // printf_vector_double(b_index, num_b);
        // printf("z = %f", z);
        // printf("\n\n\n");


        // 2
        double** max_atomic = malloc(sizeof(double)*num_x);
        for(int i = 0; i < num_x; i++){
            max_atomic[i] = malloc(sizeof(double)*num_b);
            ones_vector_double(max_atomic[i], num_b);
            k_times_double(max_atomic[i], -1.0, num_b);
        }

        // 3
        double* max_z = malloc(sizeof(double)*num_x);
        zeros_vector_double(max_z, num_x);

        // 4
        for(int i = 0; i < num_x; i++){      

            // 5      
            if(c[i] > 0){

                // 6
                for(int j = 0; j < num_b; j++){

                    if(a[j][i] > 0){
                        max_atomic[i][j] = b[j] / a[j][i];
                    }

                }

                // 7
                max_z[i] = DBL_MAX;
                for(int j = 0; j < num_b; j++){
                    if( (max_atomic[i][j] != -1.0) && (c[i]*max_atomic[i][j] < max_z[i]) ){
                        max_z[i] = c[i]*max_atomic[i][j];
                    }
                }

                // 8
                if(max_z[i] == DBL_MAX){
                    printf("Tiis problem has no solution.\n");
                    return -1;
                }
            }
        }

        // printf("M_");
        // printf_matrix_double(max_atomic, num_x, num_b);
        // printf("Mz_");
        // printf_vector_double(max_z, num_x);


        // 13, 17
        max = -DBL_MAX;
        int index_i = -1;
        for(int i = 0; i < num_x; i++){
            if( max_z[i] > max ){
                max = max_z[i];
                index_i = i;
            }
        }
        // printf("I = %d\n", index_i);

        // 14
        if(max == 0){
            break;
        }

        // 17
        min = DBL_MAX;
        int index_j = -1;
        for(int j=0; j < num_b; j++){
            if( (max_atomic[index_i][j] < min) && (max_atomic[index_i][j] != -1.0) ){
                min = max_atomic[index_i][j];
                index_j = j;
            }
        }
        // printf("J = %d\n", index_j);

        // 18
        int keep = n_index[index_i];
        n_index[index_i] = b_index[index_j];
        b_index[index_j] = keep;

        // 19
        a[index_j][index_i] = 1 / a[index_j][index_i];
        b[index_j] = b[index_j]*a[index_j][index_i];

        // 20
        for(int i = 0; i < num_x; i++){
            if(i == index_i){
                continue;
            }
            a[index_j][i] = a[index_j][i]*a[index_j][index_i];
        }

        // 21
        for(int j = 0; j < num_b; j++){
            if(j == index_j){
                continue;
            }
            for(int i = 0; i < num_x; i++){
                if(i == index_i){
                    continue;
                }
                a[j][i] = a[j][i] - a[j][index_i]*a[index_j][i];
            }
        }

        // 22
        for(int j = 0; j < num_b; j++){
            if(j == index_j){
                continue;
            }
            b[j] = b[j] - a[j][index_i]*b[index_j];
            a[j][index_i] = -a[j][index_i]*a[index_j][index_i];
        }

        // 23
        z = z + c[index_i]*b[index_j];

        // 24
        for(int i = 0; i < num_x; i++){
            if(i == index_i){
                continue;
            }
            c[i] = c[i] - c[index_i]*a[index_j][i];
        }

        // 25
        c[index_i] = -c[index_i]*a[index_j][index_i];



        free(max_z);

        for(int i = 0; i < num_x; i++){
            free(max_atomic[i]);
        }
        free(max_atomic);

        

    }

    double* sol = malloc(sizeof(double)*(num_x + num_b));

    for(int i = 0; i < num_x; i++){
        sol[(int)n_index[i]] = 0.0;
    }
    for(int j = 0; j < num_b; j++){
        sol[(int)b_index[j]] = b[j];
    }

    // printf_vector_double(n_index, num_x);
    // printf_vector_double(b_index, num_b);
    // printf("\n\n\n");

    printf("\n\n");

    printf_vector_double(sol, num_x+num_b);

    printf("\n");
    printf("real_solution = \n");
    for(int i = 0; i < num_x; i++){
        printf(" %f  ", sol[i]);
    }
    printf("\n\n");
    printf("obj = %f", z);

    printf("\n\n");

    free(sol);
    free(n_index);
    free(b_index);

    return 0;

}